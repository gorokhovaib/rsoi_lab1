package ru.bmstu.rsoi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.JsonPath;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.bmstu.rsoi.model.AddPersonRequest;
import ru.bmstu.rsoi.web.PersonsController;
import ru.bmstu.rsoi.model.PersonInfoResponse;
import ru.bmstu.rsoi.service.PersonsService;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonsController.class)
public class PersonsControllerTest {
    private static final Integer I1 = 1;
    private static final Integer I2 = 1;
    private static final String F1 = "First1";
    private static final String F2 = "First2";
    private static final String L1 = "Last1";
    private static final String L2 = "Last2";
    private static final Boolean W1 = true;
    private static final Boolean W2 = false;



    @Autowired
    private MockMvc mvc;

    @MockBean
    private PersonsService personsService;

    @Test
    public void getAllPersons()
            throws Exception {
        PersonInfoResponse person1 = new PersonInfoResponse()
                .setId(I1)
                .setPersonUid(UUID.randomUUID())
                .setFirstName(F1)
                .setLastName(L1)
                .setIsWorking(W1);

        PersonInfoResponse person2 = new PersonInfoResponse()
                .setId(I2)
                .setPersonUid(UUID.randomUUID())
                .setFirstName(F2)
                .setLastName(L2)
                .setIsWorking(W2);

        List<PersonInfoResponse> persons = Arrays.asList(person1, person2);

        when(personsService.getAllPersons()).thenReturn(persons);

        mvc.perform(get("/persons")
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].firstName").value(F1))
                .andExpect(jsonPath("$[0].lastName").value(L1))
                .andExpect(jsonPath("$[0].isWorking").value(W1))
                .andExpect(jsonPath("$[1].firstName").value(F2))
                .andExpect(jsonPath("$[1].lastName").value(L2))
                .andExpect(jsonPath("$[1].isWorking").value(W2))
                .andExpect(jsonPath("$[2]").doesNotExist());
    }

    @Test
    public void GetPersonByIdSuccess()
            throws Exception {
        PersonInfoResponse person1 = new PersonInfoResponse()
                .setId(I1)
                .setPersonUid(UUID.randomUUID())
                .setFirstName(F1)
                .setLastName(L1)
                .setIsWorking(W1);


        when(personsService.getPersonInfo(I1)).thenReturn(person1);

        mvc.perform(get("/persons/" + I1.toString())
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.firstName").value(F1))
                .andExpect(jsonPath("$.lastName").value(L1))
                .andExpect(jsonPath("$.isWorking").value(W1));
    }

    @Test
    public void AddPerson()
        throws Exception {
        AddPersonRequest request = new AddPersonRequest()
                .setFirstName(F1)
                .setLastName(L1)
                .setIsWorking(W1);

        PersonInfoResponse person1 = new PersonInfoResponse()
                .setId(I1)
                .setPersonUid(UUID.randomUUID())
                .setFirstName(F1)
                .setLastName(L1)
                .setIsWorking(W1);

        when(personsService.addPerson(request)).thenReturn(person1);

        mvc.perform(post("/persons")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"firstName\": \""+F1+"\", " +
                        "\"lastName\": \""+L1+"\", " +
                        "\"isWorking\": "+W1.toString() + "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(F1))
                .andExpect(jsonPath("$.lastName").value(L1));
    }



}
