package ru.bmstu.rsoi.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class PersonInfoResponse {
    private Integer id;
    private UUID personUid;
    private String firstName;
    private String lastName;
    private Boolean isWorking;

}
