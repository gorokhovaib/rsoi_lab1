package ru.bmstu.rsoi.model;

import com.google.common.base.MoreObjects;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class AddPersonRequest {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private Boolean isWorking;

    @Override
    public String toString() {
        return MoreObjects
                .toStringHelper(this)
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("isWorking", isWorking)
                .toString();
    }
}

