package ru.bmstu.rsoi.service;

import ru.bmstu.rsoi.model.AddPersonRequest;
import ru.bmstu.rsoi.model.PersonInfoResponse;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.UUID;

public interface PersonsService {
    @Nonnull
    PersonInfoResponse addPerson(@Nonnull AddPersonRequest request);

    @Nonnull
    PersonInfoResponse getPersonInfo(@Nonnull Integer id);

    @Nonnull
    List<PersonInfoResponse> getAllPersons();

    @Nonnull
    PersonInfoResponse updatePerson(@Nonnull AddPersonRequest request,
                                    @Nonnull Integer id);
}
