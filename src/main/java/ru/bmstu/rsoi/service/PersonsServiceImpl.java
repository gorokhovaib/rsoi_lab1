package ru.bmstu.rsoi.service;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bmstu.rsoi.domain.Person;
import ru.bmstu.rsoi.model.AddPersonRequest;
import ru.bmstu.rsoi.model.PersonInfoResponse;
import ru.bmstu.rsoi.repository.PersonsRepository;

import javax.annotation.Nonnull;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.slf4j.LoggerFactory.getLogger;

@Service
public class PersonsServiceImpl
        implements PersonsService {
    private static final Logger logger = getLogger(PersonsServiceImpl.class);
    private final PersonsRepository personsRepository;

    @Autowired
    public PersonsServiceImpl(PersonsRepository personsRepository) {
        this.personsRepository = personsRepository;
    }

    @Nonnull
    @Override
    @Transactional
    public PersonInfoResponse addPerson(@Nonnull AddPersonRequest request) {
        logger.info(request.toString());

        final Person person = buildPerson(request);

        logger.info(person.toString());

        personsRepository.save(person);

        logger.info("Added new person with name '{}'", person.getFirstName());

        return buildPersonInfoResponse(person);
    }

    @Nonnull
    @Override
    public PersonInfoResponse getPersonInfo(@Nonnull Integer id) {
        return personsRepository
                .findById(id)
                .map(this::buildPersonInfoResponse)
                .orElseThrow(() -> new EntityNotFoundException(format("No person with id '%s'", id)));
    }

    @Nonnull
    @Override
    public List<PersonInfoResponse> getAllPersons() {
        return personsRepository.findAll()
                .stream()
                .map(this::buildPersonInfoResponse)
                .collect(Collectors.toList());
    }

    @Nonnull
    private Person buildPerson(@Nonnull AddPersonRequest request) {
        return new Person()
                .setPersonUid(UUID.randomUUID())
                .setFirstName(request.getFirstName())
                .setLastName(request.getLastName())
                .setIsWorking(request.getIsWorking());
    }

    @Nonnull
    private PersonInfoResponse buildPersonInfoResponse(@Nonnull Person person) {
        return new PersonInfoResponse()
                .setId(person.getId())
                .setPersonUid(person.getPersonUid())
                .setFirstName(person.getFirstName())
                .setLastName(person.getLastName())
                .setIsWorking(person.getIsWorking());
    }

    @Nonnull
    @Override
    @Transactional
    public PersonInfoResponse updatePerson(@Nonnull AddPersonRequest request,
                                            @Nonnull Integer id) {

        final Person person = buildPerson(request);

        Person p = personsRepository.getOne(id);
        p.setPersonUid(person.getPersonUid())
                .setFirstName(person.getFirstName())
                .setLastName(person.getLastName())
                .setIsWorking(person.getIsWorking());
        personsRepository.save(p);

        logger.info("Updated person with name '{}'", person.getFirstName());

        return buildPersonInfoResponse(person);
    }
}
