package ru.bmstu.rsoi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bmstu.rsoi.domain.Person;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.UUID;

public interface PersonsRepository
    extends JpaRepository<Person, Integer> {

    Optional<Person> findById(@Nonnull Integer id);




}
