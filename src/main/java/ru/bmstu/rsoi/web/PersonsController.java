package ru.bmstu.rsoi.web;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.bmstu.rsoi.model.AddPersonRequest;
import ru.bmstu.rsoi.model.PersonInfoResponse;
import ru.bmstu.rsoi.service.PersonsService;

import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("/persons")
public class PersonsController {
    private static final Logger logger = getLogger(PersonsController.class);
    private final PersonsService personsService;

    @Autowired
    public PersonsController(PersonsService personsService) {
        this.personsService = personsService;
    }

    @GetMapping(
            path = "/{id}",
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public PersonInfoResponse getPersonInfo(@PathVariable Integer id) {
        return personsService.getPersonInfo(id);
    }

    @GetMapping(
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public List<PersonInfoResponse> getPersons() {
        return personsService.getAllPersons();
    }

    @PostMapping(
            consumes = APPLICATION_JSON_UTF8_VALUE,
            produces = APPLICATION_JSON_UTF8_VALUE
    )
    public PersonInfoResponse addPerson(@RequestBody AddPersonRequest request) {
        logger.info(personsService.toString());
        return personsService.addPerson(request);
    }

    @PutMapping(
            path = "/{id}",
            produces = APPLICATION_JSON_UTF8_VALUE,
            consumes = APPLICATION_JSON_UTF8_VALUE
    )
    public PersonInfoResponse updatePerson(@RequestBody AddPersonRequest request,
                                           @PathVariable Integer id) {
        return personsService.updatePerson(request, id);
    }

}
